# MotionSonif: Android App for tablet
Contacts:
- danilo.spada@harmonio.org  // project administration and infos
- debernardi.emanuele@gmail.com // software and installation issues
- masciadri.andrea@gmail.com // software and installation issues

# Installation notes:
- Download the apk on the Android Tablet
- Open the apk to install it; the operating system will ask for permissions to use the application: accept the conditions to install the app.

# App usage
- Before to start the app, you will have to configure the tablet in order to create a WiFi Hotspot. Access the Tablet's setting and configure the hotspot with the following properties: hotspot name -> c4cHotspot , hotspot password -> c4cPassword
- Activate the hotspot
- Launch the application
- Turn on the wearable devices; the devices will automatically connect to the Tablet's hotspot
- On the application home screen it is possible to choose whether to perform the "exercises of the day" (a random sequence of all the exercises, with a duration of 60 seconds including the 16-second video delivery), or a "free exercise" from those available (with no limit of duration)

# Exercises
- Silence, walking march
- Silence, side steps
- Silence, rotation steps
- Forced rythm, walking march
- Forced rythm, side steps
- Forced rythm, rotation steps
- Forced Melody, walking march
- Forced Melody, side steps
- Forced Melody, rotation steps
- Sonified rythm, walking march
- Sonified rythm, side steps
- Sonified rythm, rotation steps
- Sonified Melody, walking march
- Sonified Melody, side steps
- Sonified Melody, rotation steps
